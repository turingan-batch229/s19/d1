// console.log("Hello World!");

// [SECTION] - if, else if, else statement

let numG = -1;

// if statement
// executes a statement if a specified condition is true

// This will not execute since the condition is not met or it is "false"
 if(numG < -5){
 	console.log("Hello");
 } 

// This will execute since the condition was met or it is "true"
  if(numG < 1){
 	console.log("Hello");
 } 

// else if statement
 /*
	1. Executes a statement if previous conditions are falls and if the specified condition is true.
	2. The "else if" clause is optional and can be added to capture additional condition to change the flow of a program
 */

let numH = 1

if(numG > 0){
	console.log("Hello");
}else if (numH > 0){
	console.log("World");
}

// else statement
/*
1. Executes the statement if all other conditions are "false".
2. The "else" statement is optional and can be added to capture any other result to change the flow of the program.
*/

if (numG > 0) {
	console.log("Hello")
}else if(numH == 0){
	console.log("World")
}else{
	console.log("Again")
}

// if, else if and else statement with functions

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return "Not a typhoon yet.";
	}else if(windSpeed <= 61){
		return "Tropical depression detected.";
	}else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Storm Detected.";
	}else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Storm Detected.";
	}else{
		return "Typhoon Detected.";
	}
}


// Return the string to the variable "message" that invoke it.
message = determineTyphoonIntensity(70);
console.log(message);

if (message == "Tropical Storm Detected.") {
	console.warn(message);
}

message = determineTyphoonIntensity(112);
console.log(message);

message = determineTyphoonIntensity(120);
console.log(message);


// [SECTION] - Truthy and Falsy
/* 
    - In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
    - Values are considered true unless defined otherwise
    - Falsy values/exceptions for truthy:
        1. false
        2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN
*/

// Truthy examples
if (true) {
	console.log("truthy");
}

if (1) {
	console.log("truthy");
}

if ([]) {
	console.log("truthy");
}

// Falsy Examples

if (false) {
	console.log("falsy");
}

if (0) {
	console.log("falsy");
}

if (undefined) {
	console.log("falsy");
}

// [SECTION] - Conditional (Ternary) Operator
/* 
    - The Conditional (Ternary) Operator takes in three operands:
        1. condition
        2. expression to execute if the condition is truthy
        3. expression to execute if the condition is falsy
    - Can be used as an alternative to an "if else" statement
    - Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
    - Commonly used for single statement execution where the result consists of only one line of code
    - For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
    - Syntax
        (expression) ? ifTrue : ifFalse;
*/

// Single statement execution
let ternaryResult1 = (1 < 18) ? true : false; 
	console.log("Result of ternary operator: " + ternaryResult1);

let ternaryResult2 = (100 < 18) ? true : false; 
	console.log("Result of ternary operator: " + ternaryResult2);

// Multiple statement execution
// Both functions perform two separate tasks which changes the value of the "name" variable and returns the result storing it in the "legalAge" variable

/*let name;

function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge(){
	name = "Jane";
	return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
console.log(typeof age);
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of ternary operator in function: " + legalAge + ", " + name);*/

// [SECTION] - Switch Statement
/* 
    - Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
    - The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
    - The "expression" is the information used to match the "value" provided in the switch cases
	- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
    - Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
    - The "break" statement is used to terminate the current loop once a match has been found
    - Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found
    - Syntax
        switch (expression) {
            case value:
                statement;
                break;
            default:
                statement;
                break;
        }
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red");
		break; 
	case 'tuesday':
		console.log("The color of the day is orange");
		break; 
	case 'wednesday':
		console.log("The color of the day is yellow");
		break; 
	case 'thursday':
		console.log("The color of the day is green");
		break; 
	case 'friday':
		console.log("The color of the day is blue");
		break; 
	case 'saturday':
		console.log("The color of the day is indigo");
		break; 
	case 'sunday':
		console.log("The color of the day is violet");
		break; 
	default:
		console.log("Please input a valid day");
		break;
}

// [SECTION] - Try-Catch-Finally Statement

function showIntensityAlert(windSpeed){
	try{
		// Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}finally{
		alert("Intensity updates will show new alert");
	}
}

showIntensityAlert(56);